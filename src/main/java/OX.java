/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
import java.util.Scanner;

public class OX {

    public static void main(String[] args) {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char player = 'X';
        int row, col;
        Scanner kb = new Scanner(System.in);

        showWelcome();
        while (true) {
            showBoard(board);
            showTurn(player);
            System.out.print("Please input row / col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (checkInput(row, col, board)) {
                putSite(row, col, board, player);
            } else {
                continue;
            }
            if (checkCurrentScore(row, col, board)) {
                break;
            }
            player = switchingSite(player);

        }
        showBoard(board);
        showWin(player, row, col, board);
        showBye();

    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showBoard(char[][] board) {
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + board[i][j]);
            }
            System.out.println();
        }
    }

    public static void showTurn(char player) {
        System.out.println("Turn " + player);
    }

    public static char switchingSite(char player) {
        if (player == 'X') {
            return 'O';
        } else {
            return 'X';
        }
    }

    public static boolean checkInput(int row, int col, char[][] board) {
        if (row < 1 || row > 3 || col < 1 || col > 3) {
            System.out.println("This col/row is not available.");
            return false;
        }
        if (board[row - 1][col - 1] == 'X' || board[row - 1][col - 1] == 'O') {
            System.out.println("This col/row is already occupied.");
            return false;
        }
        return true;
    }

    public static void putSite(int row, int col, char[][] board, char player) {
        board[row - 1][col - 1] = player;
    }

    public static boolean checkCurrentScore(int row, int col, char[][] board) {
        if (checkWin(row, col, board)) {
            return true;
        } else if (checkDraw(board)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkWin(int row, int col, char[][] board) {
        if (checkHorizon(row - 1, board) == true) {
            return true;
        } else if (checkVerticle(col - 1, board) == true) {
            return true;
        } else if (checkX1(board)) {
            return true;
        } else if (checkX2(board)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkHorizon(int row, char[][] board) {
        if (board[row][0] == board[row][1] && board[row][0] == board[row][2] && board[row][0] != '-') {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkVerticle(int col, char[][] board) {
        if (board[0][col] == board[1][col] && board[0][col] == board[2][col] && board[0][col] != '-') {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkX1(char board[][]) {
        if (board[1][1] != '-') {
            if (board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkX2(char board[][]) {
        if (board[1][1] != '-') {
            if (board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkDraw(char board[][]) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;

    }

    public static void showWin(char player, int row, int col, char[][] board) {
        if (checkWin(row, col, board)) {
            System.out.println("Player " + player + " Win...!");
        } else if (checkDraw(board)) {
            System.out.println("Draw...!");
        }
    }

    public static void showBye() {
        System.out.println("Bye Bye . . .");
    }

}
